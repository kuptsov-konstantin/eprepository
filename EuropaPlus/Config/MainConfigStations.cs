﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EuropaPlus.Config
{
	public class MainConfigStations
	{

		[JsonProperty("version")]
		public string Version { get; set; }

		[JsonProperty("stations")]
		public Dictionary<string, Station> Stations { get; set; }

		[JsonProperty("airUrl")]
		public string AirUrl { get; set; }

		[JsonProperty("programsUrl")]
		public string ProgramsUrl { get; set; }

		[JsonProperty("djsUrl")]
		public string DjsUrl { get; set; }

		[JsonProperty("noveltiesUrl")]
		public string NoveltiesUrl { get; set; }

		[JsonProperty("videoUrl")]
		public string VideoUrl { get; set; }
	}




}