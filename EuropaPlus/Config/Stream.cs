﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EuropaPlus.Config
{

	public class Stream
	{

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("bitrate")]
		public int Bitrate { get; set; }

		[JsonProperty("title")]
		public string Title { get; set; }

		[JsonProperty("url")]
		public string Url { get; set; }

		[JsonProperty("default")]
		public bool Default { get; set; }
	}

}


