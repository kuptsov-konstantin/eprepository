﻿using Newtonsoft.Json;

namespace EuropaPlus.Config
{

	public class Logos
	{

		[JsonProperty("122x60")]
		public string Logos122x60 { get; set; }

		[JsonProperty("244x120")]
		public string Logos244x120 { get; set; }

		[JsonProperty("366x180")]
		public string Logos366x180 { get; set; }

		[JsonProperty("70x34")]
		public string Logos70x34 { get; set; }

		[JsonProperty("140x68")]
		public string Logos140x68 { get; set; }

		[JsonProperty("210x102")]
		public string Logos210x102 { get; set; }
	}


}
