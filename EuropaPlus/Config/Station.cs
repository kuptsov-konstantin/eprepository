﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace EuropaPlus.Config
{
	public class Station
	{

		[JsonProperty("default")]
		public bool Default { get; set; }

		[JsonProperty("type")]
		public string Type { get; set; }

		[JsonProperty("title")]
		public string Title { get; set; }

		[JsonProperty("playlistUrl")]
		public string PlaylistUrl { get; set; }

		[JsonProperty("shortPlaylistUrl")]
		public string ShortPlaylistUrl { get; set; }

		[JsonProperty("logos")]
		public Logos logos { get; set; }

		[JsonProperty("streams")]
		public IList<Stream> Streams { get; set; }

		[JsonProperty("sort")]
		public int Sort { get; set; }
	}

}


