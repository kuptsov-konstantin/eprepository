﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EuropaPlus.Config;

namespace EuropaPlus
{

	public class PlayerData
	{
		private MainConfigStations _mainConfigStations;
		public event EventHandler<Events.LoadedEventArgs> Loaded;
		public void Load()
		{

			var task = Task.Factory.StartNew(async () =>
			{

				_mainConfigStations = await LoadConfigDataAsync();
				Loaded?.Invoke(this, new Events.LoadedEventArgs() { MainConfigStations = _mainConfigStations });
			});

		}
		public async Task<Config.MainConfigStations> LoadConfigDataAsync()
		{
			using (WebClient webClient = new WebClient())
			{
				var data = await webClient.DownloadStringTaskAsync(Config.EuropaPlusConfigData.ConfigUri);
				return Newtonsoft.Json.JsonConvert.DeserializeObject<Config.MainConfigStations>(data);
			}
		}

	}
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		Config.MainConfigStations MainConfigStations = null;
		PlayerData _playerData;
		public MainWindow()
		{
			InitializeComponent();
			_playerData = new PlayerData();
			_playerData.Loaded += playerData_Loaded;
			_playerData.Load();
		}

		private void playerData_Loaded(object sender, Events.LoadedEventArgs e)
		{
			MainConfigStations = e.MainConfigStations;
			foreach (var item in MainConfigStations.Stations)
			{
				StationGrid station = new StationGrid(item);
				StationsPanel.Children.Add(station);

			}
			
		}
	}
}