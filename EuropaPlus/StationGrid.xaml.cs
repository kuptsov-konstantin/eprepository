﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using EuropaPlus.Config;

namespace EuropaPlus
{
	/// <summary>
	/// Логика взаимодействия для StationGrid.xaml
	/// </summary>
	public partial class StationGrid : Page
	{
		private KeyValuePair<string, Station> item;

		public StationGrid()
		{
			InitializeComponent();
		}

		public StationGrid(KeyValuePair<string, Station> item) : this()
		{
			this.item = item;
			StationName.Content = item.Value.Title;
		}
	}
}
