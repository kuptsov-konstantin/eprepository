﻿using System;
using EuropaPlus.Config;

namespace EuropaPlus
{
	namespace Events
	{
		public class LoadedEventArgs : EventArgs
		{
			public MainConfigStations MainConfigStations { get; set; }
		}
	}
}